require "optparse"
require File.expand_path("../base", __FILE__)

module SaharaKvm
  module Command
    class Save < Base
      def execute

        options = {}
        opts = OptionParser.new do |opts|
          opts.banner = "Enters sandbox state"
          opts.separator ""
          opts.separator "Usage: vagrant sandbox-kvm save snapshot-name <vmname>"
        end

        # Parse the options
        argv = parse_options(opts)
        return if !argv

        snapshot_name = argv.shift
        raise ArgumentError, "snapshot-name not defined." if !snapshot_name || has_target_vms?(snapshot_name) 

        each_virtual_machine(argv) do |machine,domain_name|
          command = "vagrant halt #{machine.name}"
          puts "start #{machine.name} shutdown (#{command})"
          system(command)

          command = "virsh snapshot-create-as #{domain_name} #{snapshot_name}" 
          puts "start #{machine.name} snapshot (#{command})"
#IRB.start_session(binding)
          system(command)
        end

      end

    end
  end
end
