require "optparse"
require File.expand_path("../base", __FILE__)

module SaharaKvm
  module Command
    class Load < Base
      def execute

        options = {}
        opts = OptionParser.new do |opts|
          opts.banner = "Enters sandbox state"
          opts.separator ""
          opts.separator "Usage: vagrant sandbox-kvm load snapshot-name <vmname>"
        end

        # Parse the options
        argv = parse_options(opts)
        return if !argv

        snapshot_name = argv.shift
        raise ArgumentError, "snapshot-name not defined." if !snapshot_name || has_target_vms?(snapshot_name) 

        each_virtual_machine(argv) do |machine, domain_name|
          command = "virsh snapshot-revert #{domain_name} #{snapshot_name} --force" 
          puts "start #{machine.name} revert snapshot (#{command})"
#IRB.start_session(binding)
          system(command)
        end

      end

    end
  end
end
