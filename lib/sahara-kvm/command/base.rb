require "optparse"

module SaharaKvm
  module Command
    class Base < Vagrant.plugin("2", :command)

      def get_domain_name dominfo
        dominfo.each_line do |line|
          if line.start_with? "Name:"
            return line.split(":")[1].strip
          end
        end
      end

      def has_target_vms?(name) 
         begin
           with_target_vms([name], :reverse => true, :provider => :libvirt) do |machine|
             return true
           end
           return false
          rescue Vagrant::Errors::MachineNotFound => e
            return false
          end
      end

      def each_virtual_machine argv
        with_target_vms(argv, :reverse => true, :provider => :libvirt) do |machine|

          uuid = machine.id 
          unless uuid
            puts "not found libvirt virtual os #{machine.name}."
            next
          end

          ENV["LANG"]="C"
          dominfo = `virsh dominfo #{uuid}`
          unless $?.success?
            puts "not found libvirt virtual os #{machine.name}."
            next
          end 
          domain_name = get_domain_name dominfo

          yield machine, domain_name
        end
      end

    end
  end
end
