require "optparse"
require File.expand_path("../base", __FILE__)

module SaharaKvm
  module Command
    class Status < Base
      def execute

        options = {}
        opts = OptionParser.new do |opts|
          opts.banner = "Enters sandbox state"
          opts.separator ""
          opts.separator "Usage: vagrant sandbox-kvm status <vmname>"
        end

        # Parse the options
        argv = parse_options(opts)
        return if !argv

        each_virtual_machine(argv) do |machine,domain_name|
          command = "virsh snapshot-list #{domain_name}" 
          puts "Status #{machine.name}:#{command}"
#IRB.start_session(binding)
          system(command) 
        end
      end
    end
  end
end
