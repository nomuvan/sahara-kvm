# -*- encoding: utf-8 -*-
# vim: set fileencoding=utf-8

require "vagrant"

module SaharaKvm 
  class Plugin < Vagrant.plugin("2")
    name "sahara-kvm"
    description <<-DESC
    Sahara KVM
    DESC

    command("sandbox-kvm") do
      require File.expand_path("../sahara-kvm/command/root", __FILE__)
      Command::Root
    end
  end
end
