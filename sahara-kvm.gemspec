# -*- encoding: utf-8 -*-
require File.expand_path("../lib/sahara-kvm/version", __FILE__)

Gem::Specification.new do |s|
  s.name        = "sahara-kvm"
  s.version     = SaharaKvm::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Nomura"]
  s.email       = ["nomura@kickmogu.com"]
  s.homepage    = "http://hoge/"
  s.summary     = %q{Vagrant box creation}
  s.description = %q{Allows you to sandbox(kvm) your vagrant}

  s.required_rubygems_version = ">= 1.3.6"
  s.rubyforge_project         = "sahara-kvm"

  s.add_dependency "popen4", "~> 0.1.2"
  s.add_dependency "thor", "> 0.14"
#  s.add_dependency "systemu"

  s.add_development_dependency "bundler", ">= 1.0.0"

  s.files        = `git ls-files`.split("\n")
  s.executables  = `git ls-files`.split("\n").map{|f| f =~ /^bin\/(.*)/ ? $1 : nil}.compact
  s.require_path = 'lib'
end
